package demo.service.demoservice.controllers;

import demo.service.demoservice.dto.request.OrderItemsRequest;
import demo.service.demoservice.dto.request.OrderRequest;
import demo.service.demoservice.dto.response.OrderResponse;
import demo.service.demoservice.exceptions.WrongDateRangeException;
import demo.service.demoservice.services.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/v1/orders")
@Api(value = "orderController", description = "Endpoint for order operations")
public class OrderController {

    private static final int MAX_PAGE_SIZE = 100;
    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @SuppressWarnings("unused")
    @GetMapping
    @ApiOperation(value = "Retrieves a list of paginated orders by start and end date-time. The maximum page size is 100")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Orders successfully retrieved"),
            @ApiResponse(code = 400, message = "The input is invalid. See the returning entity for more info"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public ResponseEntity<List<OrderResponse>> search(
            @RequestParam(name = "start") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Instant start,
            @RequestParam(name = "end") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Instant end,
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "100") int size) {
        if (page < 0) {
            page = 0;
        }
        if (size <= 0 || size > MAX_PAGE_SIZE) {
            size = MAX_PAGE_SIZE;
        }
        if (start.isAfter(end)) throw new WrongDateRangeException();
        log.info("search called with start: {} and end: {}", start, end);
        return ResponseEntity.ok(orderService.getAllFromStartToEnd(start, end, PageRequest.of(page, size)));
    }

    @SuppressWarnings("unused")
    @PostMapping
    @ApiOperation(value = "Creates an order. The items represents products, they must be previously created")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Order created"),
            @ApiResponse(code = 400, message = "The input is invalid. See the returning entity for more info"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public ResponseEntity<OrderResponse> place(@Valid @RequestBody OrderRequest orderRequest) {
        log.info("place called: {}", orderRequest);
        return ResponseEntity.ok(orderService.create(orderRequest));
    }

    @SuppressWarnings("unused")
    @PatchMapping("/{id}")
    @ApiOperation(value = "Partially updates an order (only its items). " +
            "It's possible to remove items indicating qty = 0. " +
            "Existing item prices can be updated to the latest price by indicating updatePrice=true")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Order updated"),
            @ApiResponse(code = 400, message = "The input is invalid. See the returning entity for more info"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public ResponseEntity<OrderResponse> update(
            @PathVariable("id") String id,
            @Valid @RequestBody OrderItemsRequest orderItemsRequest,
            @RequestParam(name = "updatePrice", required = false, defaultValue = "false") boolean updatePrice
    ) {
        log.info("order called with id: {}, and updatePrice: {} body: {}", id, orderItemsRequest, updatePrice);
        return ResponseEntity.ok(
                orderService.update(
                        id,
                        OrderRequest.builder().items(orderItemsRequest.getItems()).build(),
                        updatePrice
                )
        );
    }
}
