package demo.service.demoservice.controllers;

import demo.service.demoservice.dto.request.ProductRequest;
import demo.service.demoservice.dto.response.ProductResponse;
import demo.service.demoservice.services.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/v1/products")
@Api(value = "productController", description = "Endpoint for product operations")
public class ProductController {

    private static final int MAX_PAGE_SIZE = 100;
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @SuppressWarnings("unused")
    @PostMapping
    @ApiOperation(value = "Creates a product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Product successfully created"),
            @ApiResponse(code = 400, message = "The input is invalid. See the returning entity for more info"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public ResponseEntity<ProductResponse> create(@Valid @RequestBody ProductRequest productRequest) {
        log.info("create called: {}", productRequest);
        return ResponseEntity.ok(productService.create(productRequest));
    }

    @SuppressWarnings("unused")
    @PutMapping("/{id}")
    @ApiOperation(value = "Updates a product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Product successfully updated"),
            @ApiResponse(code = 400, message = "The input is invalid. See the returning entity for more info"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public ResponseEntity<ProductResponse> update(@Valid @RequestBody ProductRequest productRequest,
                                                  @PathVariable(name = "id") String id) {
        log.info("update called with id {}: {}",id, productRequest);
        return ResponseEntity.ok(productService.update(productRequest.toBuilder().id(id).build()));
    }

    @SuppressWarnings("unused")
    @GetMapping
    @ApiOperation(value = "Retrieves a list of paginated products. The maximum page size is 100")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Products successfully retrieved"),
            @ApiResponse(code = 400, message = "The input is invalid. See the returning entity for more info"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public ResponseEntity<List<ProductResponse>> getAll(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "100") int size
    ) {
        if (page < 0) {
            page = 0;
        }
        if (size <= 0 || size > MAX_PAGE_SIZE) {
            size = MAX_PAGE_SIZE;
        }
        log.info("getAll called");
        return ResponseEntity.ok(productService.getAll(PageRequest.of(page, size)));
    }
}
