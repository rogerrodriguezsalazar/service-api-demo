package demo.service.demoservice.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Builder(toBuilder = true)
@Data
@Document("products")
public class Product {

    @Id
    private String id;

    private String name;

    private BigDecimal price;

    @Version
    private Long version;

}
