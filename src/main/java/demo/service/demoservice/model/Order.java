package demo.service.demoservice.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Builder(toBuilder = true)
@Data
@Document("orders")
public class Order {

    @Id
    private String id;

    private String buyerEmail;

    @Indexed
    private Instant purchaseTime;

    @Builder.Default
    private List<Item> items=new ArrayList<>();

    private BigDecimal total;

    @Version
    private Long version;

    public Order calculateTotal() {
        if (items!=null) {
            total = items.stream()
                    .map(
                            //product price * qty = item price
                            item -> item.getPrice().multiply(new BigDecimal(item.getQty()))
                    )
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
        } else {
            total = BigDecimal.ZERO;
        }
        return this;
    }
}
