package demo.service.demoservice.model;

import lombok.*;

import java.math.BigDecimal;

@Builder(toBuilder = true)
@Data
public class Item {

    private String id;

    private String name;

    private BigDecimal price;

    private Integer qty;

}
