package demo.service.demoservice.services;

import demo.service.demoservice.dto.request.ItemRequest;
import demo.service.demoservice.dto.request.OrderRequest;
import demo.service.demoservice.dto.response.ItemResponse;
import demo.service.demoservice.dto.response.OrderResponse;
import demo.service.demoservice.dto.response.ProductResponse;
import demo.service.demoservice.exceptions.OrderNotFoundException;
import demo.service.demoservice.exceptions.ProductNotFoundException;
import demo.service.demoservice.model.Item;
import demo.service.demoservice.model.Order;
import demo.service.demoservice.persistence.OrderRepository;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class OrderService {

    private final OrderRepository orderRepository;

    private final ProductService productService;

    @Autowired
    public OrderService(OrderRepository orderRepository, ProductService productService) {
        this.orderRepository = orderRepository;
        this.productService = productService;
    }

    private static Order buildOrder(OrderRequest orderRequest) {
        return Order.builder()
                .buyerEmail(orderRequest.getBuyerEmail())
                .purchaseTime(orderRequest.getPurchaseTime())
                .items(
                        squashDuplicates(orderRequest.getItems())
                )
                .id(UUID.randomUUID().toString())
                .build();
    }

    private static List<Item> squashDuplicates(List<ItemRequest> items) {
        return new ArrayList<>(items
                .stream()
                //transform each dto into a model object
                .map(
                        itemRequest ->
                                Item.builder()
                                        .id(itemRequest.getId())
                                        .qty(itemRequest.getQty())
                                        .build()
                )
                //squash quantities by adding items with the same id
                .collect(
                        Collectors.toMap(
                                Item::getId,
                                Function.identity(),
                                (a, b) -> a.toBuilder().qty(a.getQty() + b.getQty()).build()
                        )
                ).values());
    }

    public OrderResponse create(OrderRequest orderRequest) {
        val newOrder = buildOrder(orderRequest);
        val savedOrder = orderRepository.save(newOrder.toBuilder()
                .items(fetchProducts(newOrder.getItems()))
                .build()
                .calculateTotal());
        return buildOrderResponse(savedOrder);
    }

    private OrderResponse buildOrderResponse(Order order) {
        return OrderResponse.builder()
                .id(order.getId())
                .buyerEmail(order.getBuyerEmail())
                .purchaseTime(order.getPurchaseTime())
                .total(order.getTotal())
                .items(
                        order.getItems().stream().map(
                                item -> ItemResponse.builder()
                                        .qty(item.getQty())
                                        .price(item.getPrice())
                                        .name(item.getName())
                                        .id(item.getId())
                                        .build()
                        ).collect(Collectors.toList())
                ).build();
    }

    private List<Item> fetchProducts(List<Item> items) {
        if (items.isEmpty()) return new ArrayList<>();
        //build a map of <productId,item>
        val itemMap = items.stream().collect(
                Collectors.toMap(
                        Item::getId,
                        Function.identity()
                )
        );
        //fetch items with prices and set the prices into the map
        val productList = productService.getAllById(new ArrayList<>(itemMap.keySet()));
        if (productList.size() != items.size()) {
            val productIds = itemMap.keySet();
            productIds.removeAll(
                    productList.stream()
                            .map(ProductResponse::getId)
                            .collect(Collectors.toSet())
            );
            throw new ProductNotFoundException(productIds.toString());
        }
        productList.forEach(
                productResponse -> itemMap.computeIfPresent(
                        productResponse.getId(),
                        (key, oldItem) -> oldItem.toBuilder()
                                .name(productResponse.getName())
                                .price(productResponse.getPrice())
                                .build()
                )
        );
        return new ArrayList<>(itemMap.values());
    }

    public List<OrderResponse> getAllFromStartToEnd(Instant start, Instant end, Pageable pageable) {
        return orderRepository.findAllByPurchaseTimeBetween(start, end, pageable)
                .get()
                .map(this::buildOrderResponse)
                .collect(Collectors.toList());
    }

    public OrderResponse update(String id, OrderRequest orderRequest, boolean updatePrice) {
        val order = orderRepository.findById(id).orElseThrow(() -> new OrderNotFoundException(id));
        val orderItemMap = buildOrder(orderRequest).getItems().stream()
                .collect(Collectors.toMap(
                        Item::getId,
                        Function.identity()
                ));
        val items = new ArrayList<Item>();
        val itemsToUpdate = new ArrayList<Item>();
        order.getItems().forEach(
                item -> {
                    Item itemToUpdate = orderItemMap.remove(item.getId());
                    if (itemToUpdate == null) {
                        //item should not be updated regardless
                        items.add(item);
                    } else {
                        //items with qty==0 should be ignored as they will be removed
                        if (itemToUpdate.getQty() != 0) {
                            if (updatePrice) {
                                //this item should have its price and quantity updated
                                itemsToUpdate.add(item.toBuilder()
                                        .qty(itemToUpdate.getQty())
                                        .build()
                                );
                            } else {
                                //this item should only have its quantity updated
                                items.add(
                                        item.toBuilder()
                                                .qty(itemToUpdate.getQty())
                                                .build()
                                );
                            }
                        }
                    }
                }
        );
        //all remaining items are new and should be updated unless their qty is 0
        itemsToUpdate.addAll(
                orderItemMap.values().stream()
                        .filter(item -> item.getQty() != 0)
                        .collect(Collectors.toList())
        );
        //update prices and retrieve new items
        items.addAll(fetchProducts(itemsToUpdate));
        //rebuild order and calculate total
        return buildOrderResponse(
                orderRepository.save(
                        order.toBuilder()
                                .items(items)
                                .build()
                                .calculateTotal()
                )
        );
    }
}
