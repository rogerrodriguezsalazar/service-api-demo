package demo.service.demoservice.services;

import demo.service.demoservice.dto.request.ProductRequest;
import demo.service.demoservice.dto.response.ProductResponse;
import demo.service.demoservice.exceptions.ProductNotFoundException;
import demo.service.demoservice.model.Product;
import demo.service.demoservice.persistence.ProductRepository;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public ProductResponse update(ProductRequest productRequest) {
        val product = productRepository.save(
                productRepository.findById(productRequest.getId())
                        .orElseThrow(() -> new ProductNotFoundException(productRequest.getId()))
                        .toBuilder()
                        .name(productRequest.getName())
                        .price(productRequest.getPrice())
                        .build()
        );
        return buildProductResponse(product);
    }

    public ProductResponse create(ProductRequest product) {
        val newProduct = productRepository.save(
                Product.builder()
                        .id(UUID.randomUUID().toString())
                        .name(product.getName())
                        .price(product.getPrice())
                        .build()
        );
        return buildProductResponse(newProduct);
    }

    public List<ProductResponse> getAll(Pageable pageable) {
        return productRepository.findAll(pageable).get().map(
                this::buildProductResponse
        ).collect(Collectors.toList());
    }

    List<ProductResponse> getAllById(List<String> ids) {
        val results = new ArrayList<ProductResponse>();
        productRepository.findAllById(ids).forEach(product ->
                results.add(buildProductResponse(product)
                )
        );
        return results;
    }

    private ProductResponse buildProductResponse(Product product) {
        return ProductResponse.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .build();
    }
}
