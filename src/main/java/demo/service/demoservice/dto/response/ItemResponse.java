package demo.service.demoservice.dto.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.math.BigDecimal;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class ItemResponse {

    @ApiModelProperty(notes = "The product id")
    private String id;

    @ApiModelProperty(notes = "The product name. Its length must be between 3 and 256 characters")
    private String name;

    @ApiModelProperty(notes = "The price of the product. It must be positive")
    private BigDecimal price;

    @ApiModelProperty(notes = "The amount of products for this item. " +
            "It must be positive or zero for removing existing products from the order")
    private Integer qty;
}
