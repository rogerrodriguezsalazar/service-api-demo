package demo.service.demoservice.dto.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class OrderResponse {

    @ApiModelProperty(notes = "The auto generated id")
    private String id;

    @ApiModelProperty(notes = "The buyer's email")
    private String buyerEmail;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @ApiModelProperty(notes = "The date and time of the order in UTC time")
    private Instant purchaseTime;

    @ApiModelProperty(notes = "The items of the order")
    private List<ItemResponse> items;

    @ApiModelProperty(notes = "The total price of the order")
    private BigDecimal total;

}
