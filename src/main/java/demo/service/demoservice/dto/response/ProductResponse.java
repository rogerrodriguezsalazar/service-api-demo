package demo.service.demoservice.dto.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.math.BigDecimal;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class ProductResponse {

    @ApiModelProperty(notes = "The auto generated id")
    private String id;

    @ApiModelProperty(notes = "The product name. Its length must be between 3 and 256 characters")
    private String name;

    @ApiModelProperty(notes = "The price of the product. It must be positive")
    private BigDecimal price;

}
