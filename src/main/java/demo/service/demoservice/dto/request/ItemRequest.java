package demo.service.demoservice.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ItemRequest {

    @NotNull
    @Size(min = 1, max = 256)
    @ApiModelProperty(notes = "The product id. The product must exist before referencing it", required = true)
    private String id;

    @NotNull
    @PositiveOrZero
    @ApiModelProperty(notes = "The amount of products for this item. " +
            "It must be positive or zero for removing existing products from the order", required = true)
    private Integer qty;

}
