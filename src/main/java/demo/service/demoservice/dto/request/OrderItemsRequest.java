package demo.service.demoservice.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderItemsRequest {

    @NotNull
    @NotEmpty
    @Valid
    @ApiModelProperty(notes = "The list of items to update in the order", required = true)
    private List<ItemRequest> items;
}
