package demo.service.demoservice.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;

@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderRequest {

    @Email
    @NotNull
    @NotEmpty
    @ApiModelProperty(notes = "The buyer's email", required = true)
    private String buyerEmail;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @NotNull
    @ApiModelProperty(notes = "The date and time of the order in UTC time", required = true)
    private Instant purchaseTime;

    @NotNull
    @NotEmpty
    @Valid
    @ApiModelProperty(notes = "The items of the order", required = true)
    private List<ItemRequest> items;

}
