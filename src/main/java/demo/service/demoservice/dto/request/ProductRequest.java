package demo.service.demoservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Builder(toBuilder = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest {

    @JsonIgnore
    private String id;

    @NotNull
    @Size(min = 3, max = 256)
    @ApiModelProperty(notes = "The product name. Its length must be between 3 and 256 characters", required = true)
    private String name;

    @NotNull
    @Positive
    @ApiModelProperty(notes = "The price of the product. It must be positive", required = true)
    private BigDecimal price;
}
