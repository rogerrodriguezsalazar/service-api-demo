package demo.service.demoservice.exceptions.handlers;

import demo.service.demoservice.exceptions.OrderNotFoundException;
import demo.service.demoservice.exceptions.ProductNotFoundException;
import demo.service.demoservice.exceptions.WrongDateRangeException;
import demo.service.demoservice.exceptions.model.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
@Slf4j
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        val errorResponse = ex.getBindingResult().getFieldErrors().stream()
                .map(fieldError ->
                        ErrorResponse.builder()
                                .message(fieldError.getDefaultMessage())
                                .field(fieldError.getField())
                                .value(fieldError.getRejectedValue() != null ? fieldError.getRejectedValue().toString() : null)
                                .build()
                )
                .findFirst()
                .orElse(ErrorResponse.builder().message(ex.getMessage()).build());
        log.error("input validation error {}", errorResponse);
        return ResponseEntity.badRequest().body(errorResponse);
    }

    @ExceptionHandler(WrongDateRangeException.class)
    public ResponseEntity<ErrorResponse> handleWrongDateException(WrongDateRangeException ex) {
        val errorResponse = ErrorResponse.builder()
                .message("Start date and time should be before end date and time")
                .build();
        log.error("input validation error {}", errorResponse);
        return ResponseEntity.badRequest().body(
                errorResponse
        );
    }

    @ExceptionHandler(OrderNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleOrderNotFoundException(OrderNotFoundException ex) {
        val errorResponse = ErrorResponse.builder()
                .message("Order not found")
                .value(ex.getId())
                .field("id")
                .build();
        log.error("input validation error {}", errorResponse);
        return ResponseEntity.badRequest().body(
                errorResponse
        );
    }

    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleProductNotFoundException(ProductNotFoundException ex) {
        val errorResponse = ErrorResponse.builder()
                .message("Product not found")
                .value(ex.getId())
                .field("id")
                .build();
        log.error("input validation error {}", errorResponse);
        return ResponseEntity.badRequest().body(
                errorResponse
        );
    }

}
