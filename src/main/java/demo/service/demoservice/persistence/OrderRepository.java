package demo.service.demoservice.persistence;

import demo.service.demoservice.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;

@Repository
public interface OrderRepository extends PagingAndSortingRepository<Order, String> {

    Page<Order> findAllByPurchaseTimeBetween(Instant start, Instant end, Pageable pageable);
}
