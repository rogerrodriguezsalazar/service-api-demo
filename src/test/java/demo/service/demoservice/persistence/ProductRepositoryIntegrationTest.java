package demo.service.demoservice.persistence;

import demo.service.demoservice.model.Product;
import lombok.val;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataMongoTest
public class ProductRepositoryIntegrationTest {

    @Autowired
    private ProductRepository productRepository;

    private Product product1;

    private Product product2;

    @Before
    public void setup() {
        product1 = Product.builder()
                .name("product name")
                .price(new BigDecimal(36))
                .build();
        product2 = Product.builder()
                .name("product name 2")
                .price(new BigDecimal(90))
                .build();
    }

    @Test
    public void shouldCreateProduct() {
        val newProduct = productRepository.save(product1);
        assertEquals(newProduct, product1);
        assertEquals(newProduct, productRepository.findById(newProduct.getId()).orElseThrow());
    }

    @Test
    public void shouldUpdateProduct() {
        val newProduct = productRepository.save(product1);
        val productToUpdate = newProduct.toBuilder().price(new BigDecimal(70)).build();
        val updatedProduct = productRepository.save(productToUpdate);
        assertEquals(updatedProduct, productRepository.findById(updatedProduct.getId()).orElseThrow());
    }

    @Test
    public void shouldFindProducts() {
        val emptyList = productRepository.findAll(PageRequest.of(0, 100)).getContent();
        assertTrue(emptyList.isEmpty());
        val newProduct1 = productRepository.save(product1);
        val newProduct2 = productRepository.save(product2);
        val resultSet = productRepository.findAll(PageRequest.of(0, 100)).stream().collect(Collectors.toSet());
        assertEquals(2, resultSet.size());
        assertTrue(resultSet.contains(newProduct1) && resultSet.contains(newProduct2));
        val resultIterator = productRepository.findAllById(Arrays.asList(newProduct1.getId(), newProduct2.getId()));
        resultIterator.forEach(prod -> assertTrue(resultSet.contains(prod)));
    }

    @After
    public void tearDown() {
        productRepository.deleteAll();
    }
}
