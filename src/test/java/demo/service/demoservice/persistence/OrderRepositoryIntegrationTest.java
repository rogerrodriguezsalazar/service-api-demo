package demo.service.demoservice.persistence;

import demo.service.demoservice.model.Item;
import demo.service.demoservice.model.Order;
import lombok.val;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataMongoTest
public class OrderRepositoryIntegrationTest {

    @Autowired
    private OrderRepository orderRepository;

    private Order order;

    @Before
    public void setup() {
        order = Order.builder()
                .purchaseTime(Instant.now().truncatedTo(ChronoUnit.MILLIS))
                .buyerEmail("email")
                .items(
                        Arrays.asList(
                                Item.builder()
                                        .name("product name")
                                        .price(new BigDecimal(36))
                                        .qty(3)
                                        .build(),
                                Item.builder()
                                        .name("product name 2")
                                        .price(new BigDecimal(90))
                                        .qty(4)
                                        .build()
                        )
                )
                .build().calculateTotal();
    }

    @Test
    public void shouldCreateOrder() {
        val newOrder = orderRepository.save(order);
        assertEquals(newOrder, order);
    }

    @Test
    public void shouldUpdateOrder() {
        val newOrder = orderRepository.save(order);
        val orderToUpdate = newOrder.toBuilder().buyerEmail("anotherEmail").build();
        val updatedOrder = orderRepository.save(orderToUpdate);
        assertEquals(orderToUpdate, updatedOrder);
        assertEquals(updatedOrder, orderRepository.findById(orderToUpdate.getId()).orElseThrow());
    }

    @Test
    public void shouldFindOrder() {
        val start = order.getPurchaseTime().minus(1, ChronoUnit.DAYS);
        val end = order.getPurchaseTime().plus(1, ChronoUnit.DAYS);
        orderRepository.save(order);
        val result = orderRepository.findAllByPurchaseTimeBetween(
                start, end, PageRequest.of(0, 100)
        ).getContent();
        assertEquals(1, result.size());
        assertEquals(order, result.get(0));
    }

    @After
    public void tearDown() {
        orderRepository.deleteAll();
    }
}
