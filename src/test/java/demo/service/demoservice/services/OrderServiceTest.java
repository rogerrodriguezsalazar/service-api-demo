package demo.service.demoservice.services;

import demo.service.demoservice.dto.request.ItemRequest;
import demo.service.demoservice.dto.request.OrderRequest;
import demo.service.demoservice.dto.response.ProductResponse;
import demo.service.demoservice.exceptions.OrderNotFoundException;
import demo.service.demoservice.exceptions.ProductNotFoundException;
import demo.service.demoservice.model.Item;
import demo.service.demoservice.model.Order;
import demo.service.demoservice.persistence.OrderRepository;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {

    private static final String PRODUCT_ID_2 = "PRODUCT_ID_2";
    private static final String PRODUCT_ID_3 = "PRODUCT_ID_3";
    private static final String ORDER_ID = "ORDER_ID";
    private static final String PRODUCT_ID = "PRODUCT_ID";
    @Mock
    private OrderRepository orderRepository;

    @Mock
    private ProductService productService;

    private OrderService orderService;

    private Instant purchaseDate = Instant.now();

    @Before
    public void setup() {
        orderService = new OrderService(orderRepository, productService);
    }

    @Test
    public void testCreate() {
        when(productService.getAllById(Collections.singletonList(PRODUCT_ID)))
                .thenReturn(Collections.singletonList(
                        ProductResponse.builder()
                                .id(PRODUCT_ID)
                                .price(new BigDecimal("30"))
                                .name("product name")
                                .build()
                ));
        val newOrder = Order.builder()
                .buyerEmail("email")
                .purchaseTime(purchaseDate)
                .items(Collections.singletonList(
                        Item.builder()
                                .id(PRODUCT_ID)
                                .name("product name")
                                .qty(2)
                                .price(new BigDecimal(30))
                                .build()
                        )
                )
                .build().calculateTotal();
        ArgumentMatcher<Order> orderArgumentMatcher = argument -> argument.getBuyerEmail().equals(newOrder.getBuyerEmail())
                && argument.getPurchaseTime().equals(newOrder.getPurchaseTime())
                && argument.getItems().equals(newOrder.getItems())
                && argument.getTotal().equals(newOrder.getTotal());
        when(orderRepository.save(argThat(orderArgumentMatcher))).thenReturn(newOrder);
        val orderCreated = orderService.create(
                OrderRequest.builder()
                        .buyerEmail("email")
                        .purchaseTime(purchaseDate)
                        .items(Arrays.asList(
                                ItemRequest.builder()
                                        .id(PRODUCT_ID)
                                        .qty(1)
                                        .build()
                                ,
                                ItemRequest.builder()
                                        .id(PRODUCT_ID)
                                        .qty(1)
                                        .build()
                                )
                        )
                        .build()
        );
        verify(orderRepository).save(argThat(orderArgumentMatcher));
        verify(productService).getAllById(Collections.singletonList(PRODUCT_ID));
        assertEquals(orderCreated.getBuyerEmail(), newOrder.getBuyerEmail());
        assertEquals(orderCreated.getPurchaseTime(), newOrder.getPurchaseTime());
        assertEquals(orderCreated.getTotal(), newOrder.getTotal());
        assertEquals(orderCreated.getItems().get(0).getName(), newOrder.getItems().get(0).getName());
        assertEquals(orderCreated.getItems().get(0).getPrice(), newOrder.getItems().get(0).getPrice());
        assertEquals(orderCreated.getItems().get(0).getQty(), newOrder.getItems().get(0).getQty());
    }

    @Test(expected = OrderNotFoundException.class)
    public void testUpdateForUnexistingOrder() {
        when(orderRepository.findById(ORDER_ID)).thenReturn(Optional.empty());
        orderService.update(ORDER_ID, OrderRequest.builder()
                .buyerEmail("email")
                .purchaseTime(purchaseDate)
                .items(Arrays.asList(
                        ItemRequest.builder()
                                .id(PRODUCT_ID)
                                .qty(1)
                                .build()
                        ,
                        ItemRequest.builder()
                                .id(PRODUCT_ID)
                                .qty(1)
                                .build()
                        )
                )
                .build(), false);
    }

    @Test(expected = ProductNotFoundException.class)
    public void testUpdateForUnexistingProductInOrder() {
        val existingOrder = Order.builder()
                .buyerEmail("email")
                .purchaseTime(purchaseDate)
                .items(Collections.singletonList(
                        Item.builder()
                                .id(PRODUCT_ID)
                                .name("product name")
                                .qty(2)
                                .price(new BigDecimal(30))
                                .build()
                        )
                )
                .build().calculateTotal();
        when(orderRepository.findById(ORDER_ID)).thenReturn(Optional.of(existingOrder));
        when(productService.getAllById(Collections.singletonList(PRODUCT_ID)))
                .thenReturn(Collections.emptyList());
        orderService.update(ORDER_ID, OrderRequest.builder()
                .buyerEmail("email")
                .purchaseTime(purchaseDate)
                .items(Arrays.asList(
                        ItemRequest.builder()
                                .id(PRODUCT_ID)
                                .qty(1)
                                .build()
                        ,
                        ItemRequest.builder()
                                .id(PRODUCT_ID)
                                .qty(1)
                                .build()
                        )
                )
                .build(), true);
    }

    @Test
    public void testUpdateWithoutPriceUpdate() {
        val existingOrder = Order.builder()
                .id(ORDER_ID)
                .buyerEmail("email")
                .purchaseTime(purchaseDate)
                .items(Arrays.asList(
                        Item.builder()
                                .id(PRODUCT_ID)
                                .name("product name")
                                .qty(2)
                                .price(new BigDecimal(30))
                                .build(),
                        Item.builder()
                                .id(PRODUCT_ID_3)
                                .name("product 3 name")
                                .qty(2)
                                .price(new BigDecimal(40))
                                .build()
                        )
                )
                .build().calculateTotal();
        when(orderRepository.findById(ORDER_ID)).thenReturn(Optional.of(existingOrder));
        when(productService.getAllById(Collections.singletonList(PRODUCT_ID_2)))
                .thenReturn(
                        Collections.singletonList(
                                ProductResponse.builder()
                                        .id(PRODUCT_ID_2)
                                        .name("product 2 name")
                                        .price(new BigDecimal(85))
                                        .build()
                        )
                );
        val orderToUpdate = Order.builder()
                .id(ORDER_ID)
                .buyerEmail("email")
                .purchaseTime(purchaseDate)
                .items(Arrays.asList(
                        Item.builder()
                                .id(PRODUCT_ID)
                                .name("product name")
                                .price(new BigDecimal(30))
                                .qty(3)
                                .build()
                        ,
                        Item.builder()
                                .id(PRODUCT_ID_2)
                                .name("product 2 name")
                                .price(new BigDecimal(85))
                                .qty(1)
                                .build()
                ))
                .build().calculateTotal();
        when(orderRepository.save(orderToUpdate)).thenReturn(orderToUpdate);
        val updatedOrder = orderService.update(ORDER_ID, OrderRequest.builder()
                .buyerEmail("email")
                .purchaseTime(purchaseDate)
                .items(Arrays.asList(
                        ItemRequest.builder()
                                .id(PRODUCT_ID)
                                .qty(3)
                                .build()
                        ,
                        ItemRequest.builder()
                                .id(PRODUCT_ID_2)
                                .qty(1)
                                .build()
                        ,
                        ItemRequest.builder()
                                .id(PRODUCT_ID_3)
                                .qty(0)
                                .build()

                        )
                )
                .build(), false);
        verify(orderRepository).findById(ORDER_ID);
        verify(productService).getAllById(Collections.singletonList(PRODUCT_ID_2));
        verify(orderRepository).save(orderToUpdate);
        assertEquals(orderToUpdate.getTotal(), updatedOrder.getTotal());
    }

    @Test
    public void testUpdateWithPriceUpdate() {
        val existingOrder = Order.builder()
                .id(ORDER_ID)
                .buyerEmail("email")
                .purchaseTime(purchaseDate)
                .items(Arrays.asList(
                        Item.builder()
                                .id(PRODUCT_ID)
                                .name("product name")
                                .qty(2)
                                .price(new BigDecimal(30))
                                .build(),
                        Item.builder()
                                .id(PRODUCT_ID_3)
                                .name("product 3 name")
                                .qty(2)
                                .price(new BigDecimal(40))
                                .build()
                        )
                )
                .build().calculateTotal();
        when(orderRepository.findById(ORDER_ID)).thenReturn(Optional.of(existingOrder));
        when(productService.getAllById(Arrays.asList(PRODUCT_ID_2, PRODUCT_ID)))
                .thenReturn(
                        Arrays.asList(
                                ProductResponse.builder()
                                        .id(PRODUCT_ID_2)
                                        .name("product 2 name")
                                        .price(new BigDecimal(85))
                                        .build(),
                                ProductResponse.builder()
                                        .id(PRODUCT_ID)
                                        .name("product name")
                                        .price(new BigDecimal(50))
                                        .build()
                        )
                );
        val orderToUpdate = Order.builder()
                .id(ORDER_ID)
                .buyerEmail("email")
                .purchaseTime(purchaseDate)
                .items(Arrays.asList(
                        Item.builder()
                                .id(PRODUCT_ID_2)
                                .name("product 2 name")
                                .price(new BigDecimal(85))
                                .qty(1)
                                .build(),
                        Item.builder()
                                .id(PRODUCT_ID)
                                .name("product name")
                                .price(new BigDecimal(50))
                                .qty(3)
                                .build()
                ))
                .build().calculateTotal();
        when(orderRepository.save(orderToUpdate)).thenReturn(orderToUpdate);
        val updatedOrder = orderService.update(ORDER_ID, OrderRequest.builder()
                .buyerEmail("email")
                .purchaseTime(purchaseDate)
                .items(Arrays.asList(
                        ItemRequest.builder()
                                .id(PRODUCT_ID)
                                .qty(3)
                                .build()
                        ,
                        ItemRequest.builder()
                                .id(PRODUCT_ID_2)
                                .qty(1)
                                .build()
                        ,
                        ItemRequest.builder()
                                .id(PRODUCT_ID_3)
                                .qty(0)
                                .build()

                        )
                )
                .build(), true);
        verify(orderRepository).findById(ORDER_ID);
        verify(productService).getAllById(Arrays.asList(PRODUCT_ID_2, PRODUCT_ID));
        verify(orderRepository).save(orderToUpdate);
        assertEquals(orderToUpdate.getTotal(), updatedOrder.getTotal());
    }

    @Test
    public void testgetAllFromStartToEnd() {
        val pageRequest = PageRequest.of(0, 100);
        when(orderRepository.findAllByPurchaseTimeBetween(purchaseDate, purchaseDate, pageRequest))
                .thenReturn(new PageImpl<>(Collections.singletonList(Order.builder().build())));
        orderService.getAllFromStartToEnd(purchaseDate, purchaseDate, pageRequest);
        verify(orderRepository).findAllByPurchaseTimeBetween(purchaseDate, purchaseDate, pageRequest);
    }
}
