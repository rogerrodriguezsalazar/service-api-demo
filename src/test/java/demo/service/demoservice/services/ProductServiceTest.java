package demo.service.demoservice.services;

import demo.service.demoservice.dto.request.ProductRequest;
import demo.service.demoservice.dto.response.ProductResponse;
import demo.service.demoservice.exceptions.ProductNotFoundException;
import demo.service.demoservice.model.Product;
import demo.service.demoservice.persistence.ProductRepository;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    private static final String PRODUCT_ID = "PRODUCT_ID";

    @Mock
    private ProductRepository productRepository;

    private ProductService productService;

    private Product product = Product.builder().id(PRODUCT_ID).build();

    private static ProductResponse buildProductResponse() {
        return ProductResponse.builder().id(PRODUCT_ID).build();
    }

    private static ProductRequest buildProductRequest() {
        return ProductRequest.builder().id(PRODUCT_ID).build();
    }

    @Before
    public void setup() {
        productService = new ProductService(productRepository);
        when(productRepository.save(any(Product.class))).thenReturn(product);
        when(productRepository.findById(PRODUCT_ID)).thenReturn(Optional.of(product));
        when(productRepository.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(Collections.emptyList()));
        when(productRepository.findAllById(
                Collections.singletonList(PRODUCT_ID))
        ).thenReturn(Collections.singletonList(product));
    }

    @Test
    public void testCreate() {
        val response = productService.create(buildProductRequest());
        verify(productRepository).save(any(Product.class));
        assertEquals(buildProductResponse(), response);
    }

    @Test
    public void testUpdate() {
        val response = productService.update(buildProductRequest());
        verify(productRepository).findById(PRODUCT_ID);
        verify(productRepository).save(product);
        assertEquals(buildProductResponse(), response);
    }

    @Test(expected = ProductNotFoundException.class)
    public void testUpdateForUnexistingProduct() {
        when(productRepository.findById(PRODUCT_ID)).thenReturn(Optional.empty());
        productService.update(buildProductRequest());
    }

    @Test
    public void testFindAll() {
        val pageable = PageRequest.of(0, 100);
        productService.getAll(pageable);
        verify(productRepository).findAll(pageable);
    }

    @Test
    public void testFindAllById() {
        val response = productService.getAllById(Collections.singletonList(PRODUCT_ID));
        verify(productRepository).findAllById(Collections.singletonList(PRODUCT_ID));
        assertEquals(Collections.singletonList(buildProductResponse()), response);
    }

}
