package demo.service.demoservice.rest;

import demo.service.demoservice.dto.request.ProductRequest;
import demo.service.demoservice.dto.response.ProductResponse;
import demo.service.demoservice.exceptions.ProductNotFoundException;
import demo.service.demoservice.services.OrderService;
import demo.service.demoservice.services.ProductService;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@EnableAutoConfiguration(
        exclude = {
                MongoAutoConfiguration.class,
                MongoRepositoriesAutoConfiguration.class,
                MongoDataAutoConfiguration.class
        }
)
public class ProductControllerIntegrationTest {

    private static final String API_V_1_PRODUCTS = "/api/v1/products";

    @Autowired
    private TestRestTemplate restTemplate;

    @MockBean
    private ProductService productService;

    @MockBean
    private OrderService orderService;

    private ProductRequest productRequest;

    private ProductResponse productResponse;

    @Before
    public void setup() {
        productRequest = ProductRequest.builder()
                .name("product name")
                .price(new BigDecimal("35"))
                .build();
        productResponse = ProductResponse.builder()
                .name("product name")
                .price(new BigDecimal("35"))
                .id(UUID.randomUUID().toString())
                .build();
        when(productService.create(productRequest)).thenReturn(productResponse);
        when(productService.update(productRequest.toBuilder().id(productResponse.getId()).build()))
                .thenReturn(productResponse);
        when(productService.getAll(PageRequest.of(0, 100)))
                .thenReturn(Collections.singletonList(productResponse));
    }

    @Test
    public void shouldCallCreateProductEndpoint() {
        val response = restTemplate.postForEntity(API_V_1_PRODUCTS, productRequest, ProductResponse.class);
        assertEquals(200, response.getStatusCodeValue());
        val pResponse = response.getBody();
        assertEquals(productResponse, pResponse);
        verify(productService).create(productRequest);
    }

    @Test
    public void shouldCallUpdateProductEndpoint() {
        val pRequest = productRequest.toBuilder().id(productResponse.getId()).build();
        RequestEntity<ProductRequest> request = RequestEntity
                .put(URI.create(API_V_1_PRODUCTS + "/" + pRequest.getId()))
                .accept(MediaType.APPLICATION_JSON)
                .body(pRequest);
        val response = restTemplate.exchange(request, ProductResponse.class);
        assertEquals(200, response.getStatusCodeValue());
        val pResponse = response.getBody();
        assertEquals(productResponse, pResponse);
        verify(productService).update(productRequest.toBuilder().id(productResponse.getId()).build());
    }

    @Test
    public void shouldCallGetAllProductEndpoint() {
        val response = restTemplate.exchange(URI.create(API_V_1_PRODUCTS),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ProductResponse>>() {
                });
        assertEquals(200, response.getStatusCodeValue());
        val pResponse = response.getBody();
        assertNotNull(pResponse);
        assertEquals(productResponse, pResponse.get(0));
        verify(productService).getAll(PageRequest.of(0, 100));
    }

    @Test
    public void shouldFailOnInvalidPrice() {
        val pRequest = productRequest.toBuilder().price(null).build();
        val response = restTemplate.postForEntity(API_V_1_PRODUCTS, pRequest, String.class);
        assertEquals(400, response.getStatusCodeValue());
    }

    @Test
    public void shouldFailOnInvalidProductName() {
        val pRequest = productRequest.toBuilder().name("").build();
        val response = restTemplate.postForEntity(API_V_1_PRODUCTS, pRequest, String.class);
        assertEquals(400, response.getStatusCodeValue());
    }

    @Test
    public void shouldFailOnProductNotFound() {
        val pRequest = productRequest.toBuilder().id("invalid_product_id").build();
        when(productService.update(pRequest)).thenThrow(new ProductNotFoundException(null));
        RequestEntity<ProductRequest> request = RequestEntity
                .put(URI.create(API_V_1_PRODUCTS + "/invalid_product_id"))
                .accept(MediaType.APPLICATION_JSON)
                .body(pRequest);
        val response = restTemplate.exchange(request, ProductResponse.class);
        assertEquals(400, response.getStatusCodeValue());
    }

}
