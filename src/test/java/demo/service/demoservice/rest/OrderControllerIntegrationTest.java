package demo.service.demoservice.rest;

import demo.service.demoservice.dto.request.ItemRequest;
import demo.service.demoservice.dto.request.OrderItemsRequest;
import demo.service.demoservice.dto.request.OrderRequest;
import demo.service.demoservice.dto.response.ItemResponse;
import demo.service.demoservice.dto.response.OrderResponse;
import demo.service.demoservice.dto.response.ProductResponse;
import demo.service.demoservice.exceptions.OrderNotFoundException;
import demo.service.demoservice.exceptions.ProductNotFoundException;
import demo.service.demoservice.exceptions.model.ErrorResponse;
import demo.service.demoservice.services.OrderService;
import demo.service.demoservice.services.ProductService;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.net.URI;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@EnableAutoConfiguration(
        exclude = {
                MongoAutoConfiguration.class,
                MongoRepositoriesAutoConfiguration.class,
                MongoDataAutoConfiguration.class
        }
)
public class OrderControllerIntegrationTest {

    private static final String API_V_1_ORDERS = "/api/v1/orders";
    private static final String ORDER_ID = "ORDER_ID";
    private static final String PRODUCT_ID = "PRODUCT_ID";

    @Autowired
    private TestRestTemplate restTemplate;

    @MockBean
    private ProductService productService;

    @MockBean
    private OrderService orderService;

    private OrderRequest orderRequest;

    private OrderResponse orderResponse;

    private OrderItemsRequest orderItemsRequest;

    private Instant dateStart;

    private Instant dateEnd;

    @Before
    public void setup() {
        orderRequest = OrderRequest.builder()
                .purchaseTime(Instant.now())
                .buyerEmail("email@domain.com")
                .items(
                        Collections.singletonList(
                                ItemRequest.builder()
                                        .id(PRODUCT_ID)
                                        .qty(5)
                                        .build()
                        )
                )
                .build();
        orderResponse = OrderResponse.builder()
                .id(UUID.randomUUID().toString())
                .purchaseTime(orderRequest.getPurchaseTime())
                .buyerEmail(orderRequest.getBuyerEmail())
                .items(
                        Collections.singletonList(
                                ItemResponse.builder()
                                        .id(PRODUCT_ID)
                                        .name("product name")
                                        .price(new BigDecimal(30))
                                        .qty(5)
                                        .build()
                        )
                )
                .total(new BigDecimal(150))
                .build();
        orderItemsRequest = OrderItemsRequest.builder()
                .items(orderRequest.getItems())
                .build();
        dateStart = orderRequest.getPurchaseTime().minus(1, ChronoUnit.DAYS);
        dateEnd = orderRequest.getPurchaseTime().plus(1, ChronoUnit.DAYS);
        when(orderService.create(orderRequest)).thenReturn(orderResponse);
        when(orderService.update(
                ORDER_ID,
                OrderRequest.builder().items(orderItemsRequest.getItems()).build(),
                false
        )).thenReturn(orderResponse);
        when(orderService.getAllFromStartToEnd(
                dateStart, dateEnd, PageRequest.of(0, 100))
        ).thenReturn(Collections.singletonList(orderResponse));
    }

    @Test
    public void shouldCallCreate() {
        val response = restTemplate.postForEntity(API_V_1_ORDERS, orderRequest, OrderResponse.class);
        assertEquals(200, response.getStatusCodeValue());
        val pResponse = response.getBody();
        assertEquals(orderResponse, pResponse);
        verify(orderService).create(orderRequest);
    }

    @Test
    public void shouldCallUpdate() {
        val pResponse = restTemplate.patchForObject(URI.create(API_V_1_ORDERS + "/" + ORDER_ID), orderItemsRequest, OrderResponse.class);
        assertEquals(orderResponse, pResponse);
        verify(orderService).update(ORDER_ID, OrderRequest.builder().items(orderItemsRequest.getItems()).build(), false);
    }

    @Test
    public void shouldCallGetAllFromStartToEnd() {
        val response = restTemplate.exchange(
                URI.create(API_V_1_ORDERS+"?start=" + dateStart.toString() + "&end=" + dateEnd.toString() ),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<OrderResponse>>() {
                }
                );
        assertEquals(200, response.getStatusCodeValue());
        val pResponse = response.getBody();
        assertNotNull(pResponse);
        assertEquals(orderResponse, pResponse.get(0));
        verify(orderService).getAllFromStartToEnd(dateStart, dateEnd, PageRequest.of(0, 100));
    }

    @Test
    public void shouldFailWhenInvalidEmail() {
        val response = restTemplate.postForEntity(
                API_V_1_ORDERS, orderRequest.toBuilder().buyerEmail("not_an_email").build(), String.class
        );
        assertEquals(400, response.getStatusCodeValue());
    }

    @Test
    public void shouldFailWhenInvalidQty() {
        val response = restTemplate.postForEntity(
                API_V_1_ORDERS,
                orderRequest.toBuilder().items(
                        Collections.singletonList(
                                orderRequest.getItems().get(0).toBuilder().qty(-1).build()
                        )
                ).build(),
                String.class
        );
        assertEquals(400, response.getStatusCodeValue());
    }

    @Test
    public void shouldFailWhenOrderNotFound() {
        when(orderService.update(
                ORDER_ID,
                OrderRequest.builder().items(orderItemsRequest.getItems()).build(),
                false
        )).thenThrow(new OrderNotFoundException(ORDER_ID));
        val pResponse = restTemplate.patchForObject(
                URI.create(API_V_1_ORDERS + "/" + ORDER_ID), orderItemsRequest, ErrorResponse.class
        );
        assertEquals("Order not found", pResponse.getMessage());
        assertEquals("id", pResponse.getField());
        assertEquals(ORDER_ID, pResponse.getValue());
    }

    @Test
    public void shouldFailWhenProductNotFound() {
        when(orderService.update(
                ORDER_ID,
                OrderRequest.builder().items(orderItemsRequest.getItems()).build(),
                false
        )).thenThrow(new ProductNotFoundException(PRODUCT_ID));
        val pResponse = restTemplate.patchForObject(
                URI.create(API_V_1_ORDERS + "/" + ORDER_ID), orderItemsRequest, ErrorResponse.class
        );
        assertEquals("Product not found", pResponse.getMessage());
        assertEquals("id", pResponse.getField());
        assertEquals(PRODUCT_ID, pResponse.getValue());
    }

    @Test
    public void shouldFailWhenStartDateAfterEndDate() {
        val response = restTemplate.exchange(
                URI.create(API_V_1_ORDERS+"?start=" + dateEnd.toString() + "&end=" + dateStart.toString() ),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<ErrorResponse>() {
                }
        );
        assertEquals(400, response.getStatusCodeValue());
        val pResponse = response.getBody();
        assertNotNull(pResponse);
        verify(orderService, never()).getAllFromStartToEnd(dateEnd, dateStart, PageRequest.of(0, 100));
    }
}
