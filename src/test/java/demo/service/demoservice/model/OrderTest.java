package demo.service.demoservice.model;

import lombok.val;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

import static junit.framework.TestCase.assertEquals;

public class OrderTest {

    @Test
    public void testCalculateTotalWhenNoItems() {
        assertEquals(BigDecimal.ZERO, Order.builder().build().calculateTotal().getTotal());
        assertEquals(BigDecimal.ZERO, Order.builder().items(new ArrayList<>()).build().calculateTotal().getTotal());
    }

    @Test
    public void testCalculateTotal() {
        val total = Order.builder()
                .items(Arrays.asList(
                        Item.builder()
                                .price(new BigDecimal("153"))
                                .qty(23)
                                .build(),
                        Item.builder()
                                .price(new BigDecimal("7896.34"))
                                .qty(55)
                                .build(),
                        Item.builder()
                                .price(new BigDecimal("0.987"))
                                .qty(314)
                                .build()
                        )
                )
                .build().calculateTotal().getTotal();
        assertEquals(new BigDecimal("438127.618"), total);
    }
}
