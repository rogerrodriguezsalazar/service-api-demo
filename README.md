# Spring Boot 2 REST API Demo

This Demo implements a Spring Boot 2 REST API to manipulate products and orders.

#### Prerequisites
  - JDK10 (should work with JDK8+)
  - Docker
  - Git

#### How to compile and run tests

First clone the repository:

 ```sh
$ git clone git@gitlab.com:rogerrodriguezsalazar/service-api-demo.git
```
Then change to the project directory and run the gradle wrapper: 

 ```sh
$ cd service-api-demo
$ ./gradlew clean build
```

It might take a while on the first run as it has to download all gradle dependencies and the embedded MongoDB server.

If you wish to compile only (without running tests), simply run:

 ```sh
$ ./gradlew clean build -x test
```

#### How to start and stop the MongoDB server

This project uses MongoDB for persistence. There is an script for downloading and running a MongoDB Docker image:

 ```sh
$ scripts/start-mongodb.sh
```

Once you're done using the API, it's better to stop the MongoDB server to save resources, there is another script to stop it:

 ```sh
$ scripts/stop-mongodb.sh
```
If you already have a MongoDB server running, feel free to change the **application.yml** file to use your own database.

#### Starting and stopping the API service

The API service can be started with the gradle wrapper as well:

 ```sh
$ ./gradlew bootRun
```

It uses port 8080 by default, if you wish to change it, you can easily do it in the **application.yml** file

Once you are done using the API, you can stop it by hitting **CTRL+C**

#### Browsing the API documentation and using it

The API uses Swagger 2 for documentation and it can also be used for manual testing. Once the API service is running, simply open your web browser and go to:

[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

You'll find all API operations, parameters and return types there with detailed documentation on how to use them, and you can run the operations by clicking the **Try it out** button, filling the parameters and clicking the **Execute** button.

#### Automated testing

This project has three types of automated tests which are run together:
 - Unit tests: Service layer (business logic) and total calculation are tested with repository mocks
 - Web controllers integration tests: Web controllers are tested within a real web server which is configured to use a random port, and a rest client which tests most known scenarios
 - Persistence integration tests: MongoDB repositories are tested by starting an embedded MongoDB server in a random port. These tests only comprise the happy path as the goal is to test the documents and custom queries, not the repositories themselves as they use dynamically generated code

#### Gitlab CI

The project contains Gitlab continuous integration configuration which will run a build on each push to the master branch

